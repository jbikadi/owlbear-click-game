import React from 'react';
import Upgrades from './components/upgrades';
import Profile from './components/profile';
import Items from './components/items';
import Combat from './components/combat';
import {attack} from './engine/attack';
import {upgradeHelper} from './engine/upgradeHelper';
import {monsterHelper, monsterLengthHelper} from './engine/monsterHelper';

class App extends React.Component {
    constructor (props) {
        super(props);
        this.state = {
            attack: 1,
            attackLevel: 1,
            autoAttack: 1,
            autoAttackLevel: 0,
            xp: 0,
            xpSpent: 0,
            level: 1,
            items: [],
            monsters: monsterHelper(0),
            monster: monsterHelper(0, true),
            hp: monsterHelper(0, true).hp,
            location: 'Grassy Plains',
            locationLevel: 1,
            locationLevelMax: monsterLengthHelper()
        }
    }

    attackClick() {
        this.setState(attack);
    }

    autoAttack(a) {
        const interval = setInterval(() => {
            this.setState(attack(this.state, a));
        }, 1000);
        return () => clearInterval(interval);
    }

    upgradesClick(upgrade) {
        this.setState(upgradeHelper(this.state, upgrade), () => {
            if (upgrade === "Auto Attack" && this.state.autoAttackLevel === 1) {this.autoAttack(true)}
        });
    }

    render() {
        const exp = this.state.xp - this.state.xpSpent;
        const attack = this.state.attack * this.state.attackLevel;
        const autoAttack = this.state.autoAttack * this.state.autoAttackLevel;
        return (
            <div className='container row'>
                <div className='col-12 p-2 border-b border-b-md-0 border-b-lg'>
                    <Combat hp={this.state.hp} name={this.state.monster.name} onClick={() => this.attackClick()} />
                </div>
                <div className='col-12 col-sm-6 p-2 border-b'>
                    <div>Stats:</div>
                    <Profile attack={attack} autoAttack={autoAttack} xp={exp} />
                    <Items items={this.state.items} />
                </div>
                <div className='col-12 col-sm-6 p-2'>
                    <div>Upgrades:</div>
                    <Upgrades xp={exp} state={this.state} onClick={(a) => this.upgradesClick(a)} />
                </div>
            </div>
        );
    }
}

export default App;