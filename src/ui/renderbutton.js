export const RenderButton = ({name, disable, btnSize, onClick}) => {
    const disabled = (disable || false) ? ('disabled') : ('');
    const btn = (btnSize || false) ? ('btn btn-' + btnSize) : ('btn');
    return (
        <button name={name} className={btn} disabled={disabled} onClick={onClick}>{name}</button>
    );
}