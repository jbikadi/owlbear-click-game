function Profile({attack, autoAttack, xp}) {
    return (
        <div className="grid-2 grid-2-end-right">
            <div>Experience:</div>
            <div>{xp}</div>
            <div>Attack:</div>
            <div>{attack}</div>
            <div>Auto Attack:</div>
            <div>{autoAttack}</div>
        </div>
    );
}

export default Profile;