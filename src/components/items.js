import {useState} from 'react';
import {RenderButton} from '../ui/renderbutton';

function Item({loop}) {
    return loop.map((e) =>
        <li key={e}>
            {e}
        </li>
    );
}

function Items({items}) {
    const [isActive, toggleClass] = useState(false);
    const disabled = items.length === 0;
    const name = (isActive) ? ('expand -') : ('expand +');
    const toggleItems = (isActive) ? ('') : ('hide');

    if (items.length < 1) {
        return;
    }

    return (
        <div>
            <h3>Items:</h3>
            <RenderButton name={name} disable={disabled} btnSize={'small'} onClick={() => toggleClass(!isActive)} />
            <ul className={toggleItems}>
                <Item loop={items} />
            </ul>
        </div>
    );
}

export default Items;