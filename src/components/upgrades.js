import {RenderButton} from '../ui/renderbutton';
import upgradeList from '../data/upgradeList';
import {levelHelper, xpCostHelper, itemsHelper} from '../engine/upgradeHelper';

const EmptyLoop = () => (<></>);

const UpgradeButtons = ({loop, xp, state, onClick}) => {
    return loop.map(({name, stats, levelRequired}) => {
        const level = levelHelper(stats);
        const xpCost = xpCostHelper(state[level], stats.xpMultiplier);
        const disabled = xp < xpCost;
        const required = (typeof(levelRequired) !== 'undefined') ? (Object.keys(levelRequired).reduce(e => e)) : (false);
        const item = itemsHelper(name, state.items);
        const local = name === 'Location' && state.locationLevel >= state.locationLevelMax;
        if ((required && levelRequired[required] > state[required] && item) || state.items.indexOf(name) > -1 || local) {
            return (<EmptyLoop key={name} />);
        } else {
            return (
                <div key={name} className={'mb-1'}>
                    <div>cost: {xpCost}</div>
                    <RenderButton name={name} disable={disabled} btnSize={'large'} onClick={() => onClick(name)} />
                </div>
            );
        }
    });
};

function Upgrades({xp, state, onClick}) {
    return (
        <>
            <UpgradeButtons loop={upgradeList} xp={xp} state={state} onClick={(a) => onClick(a)} />
        </>
    );
}

export default Upgrades;