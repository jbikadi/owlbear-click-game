import {RenderButton} from '../ui/renderbutton';

function Combat({name, hp, onClick}) {
    const button = (hp < 1) ? ('Monster Defeated') : ('Attack!');
    return (
        <>
            <div>{name}</div>
            <div>Hit points remaining: {hp}</div>
            <RenderButton name={button} onClick={() => onClick()} />
        </>
    );
}

export default Combat;