import monsters from '../data/tier_1';

export const monsterLengthHelper = () => {
    return monsters.length;
}

export const monsterHelper = (level, single) => {
    const a = level || 0;
    const r = monsters.filter((_,i) => i === a).reduce((_,e) => e);
    if (single || false) {
        return r.reduce((p,e) => p || e);
    }
    return r;
};