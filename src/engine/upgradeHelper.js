import upgradeList from '../data/upgradeList';
import { monsterHelper } from './monsterHelper';

const listFilter = (upgrade) => {
    return upgradeList.filter((e) => upgrade === e.name).reduce(e => e);
}

const statsHelper = (upgrade) => {
    return listFilter(upgrade).stats;
}

export const levelHelper = (stats) => {
    return Object.keys(stats).filter((e) => !e.match(/xpMultiplier/gm)).reduce(e => e);
}

export const itemsHelper = (upgrade, items) => {
    const item = listFilter(upgrade);
    return 'item' in item && item.item && items.indexOf(upgrade) === -1;
}

export const xpCostHelper = (a, b) => {
    return 10 + a * b;
}

export const upgradeHelper = (state, upgrade) => {
    const stats = statsHelper(upgrade);
    const level = levelHelper(stats);
    const items = state.items;
    const monsters = (level === 'locationLevel') ? (monsterHelper(state[level])) : (state.monsters);
    if (itemsHelper(upgrade, state.items)) {
        items.push(upgrade);
    }
    return {
        [level]: state[level] + 1,
        xpSpent: state.xpSpent + xpCostHelper(state[level], stats.xpMultiplier),
        items,
        monsters
    };
}