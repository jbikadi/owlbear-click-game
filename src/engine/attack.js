export const attack = (state, props) => {
    const attack = props === true ? state.autoAttack * state.autoAttackLevel : state.attack * state.attackLevel;
    const hp = state.hp - attack;
    if (state.hp < 1) {
        const monsters = state.monsters;
        const i = Math.floor(Math.random() * (monsters.length));
        const xp = state.xp + state.monster.xp;
        return {
            xp,
            monster: monsters[i],
            hp: monsters[i].hp
        }
    }
    return {
        hp
    }
}